
import java.applet.Applet;
import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.image.FilteredImageSource;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EUGEN-IOANSAVIN
 */
public class Harta extends Applet{
        int x,y;
    public void init(){
        Random a = new Random();

        x=a.nextInt(335);
        y=a.nextInt(335);
        try{
        BufferedWriter b =new BufferedWriter(new FileWriter("coordonate_cerc.txt", true));
        b.append(x+" "+y+"\n");
        b.flush();
        }
        catch(Exception error){
            System.out.println("Probleme Fisier");
        }
    }
    public void paint(Graphics e){
        System.out.println("*");
        BasicStroke creion=new BasicStroke(3.5F,BasicStroke.CAP_BUTT,BasicStroke.JOIN_ROUND);
        BufferedImage img = null;
        TexturePaint apa=null;
        TexturePaint grass=null;
        TexturePaint mt=null;
        try{
        img = ImageIO.read(new File("image.jpg"));
        Rectangle r = new Rectangle(0,0,400, 400);
        apa = new TexturePaint(img,r);
        img = ImageIO.read(new File("grass.jpg"));
        grass = new TexturePaint(img,r);
        img = ImageIO.read(new File("mt.jpg"));
        mt= new TexturePaint(img,new Rectangle(0,0,40,40));
        }
        catch(Exception error){}
        Graphics2D ecran =(Graphics2D)e;
        ecran.setPaint(grass);
        ecran.fillRect(0,0,getWidth(),getHeight());
        setSize(350,350);
        ecran.setPaint(apa);
        GeneralPath f1=new GeneralPath();
        f1.moveTo(120F,12F);
        f1.lineTo(234F,15F);
        f1.lineTo(253F,25F);
        f1.lineTo(261F,71F);
        f1.lineTo(344F,209F);
        f1.lineTo(336F,278F);
        f1.lineTo(295F,310F);
        f1.lineTo(259F,274F);
        f1.lineTo(205F,188F);
        f1.lineTo(191F,118F);
        f1.quadTo(155.5F, 65F, 140F, 56F);
        f1.quadTo(110F, 40F, 120F, 12F);
        GeneralPath m1=new GeneralPath();
        ecran.fill(f1);
        m1.moveTo(123F, 123F);
        m1.lineTo(87F, 56F);
        m1.lineTo(44F, 90F);
        m1.lineTo(73F, 110F);  
        ecran.setPaint(mt);
        ecran.fill(m1);
        GeneralPath m2 = (GeneralPath) m1.clone();
        m2.moveTo(123F, 303F);
        m2.lineTo(87F, 236F);
        m2.lineTo(44F, 270F);
        m2.lineTo(73F, 280F); 
        ecran.fill(m2);
        ecran.setColor(Color.RED);
        ecran.setStroke(creion);
        ecran.drawOval(x, y, 15, 15);
    }
}
