import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class T18 extends Applet implements ActionListener {
    LocalTime momentx,momentOprire;
    String nume;
    Font fontTip1 = new Font("Verdana", Font.BOLD, 20);
    Font fontTip2 = new Font("Calibri", Font.BOLD, 30);
    Font fontTip3 = new Font("Vladimir Script", Font.BOLD, 40);
    FontMetrics fm = getFontMetrics(fontTip3);                
    Label label1, label2, label3, label4, label5, label6, label7, label8,label9,label10,emp;
    TextField intrare;
    Button continuare;
    int numarAleator=0, numarPropus=0, incercari=5,i=0;
    LocalDateTime dataAziCompleta = LocalDateTime.now();
    LocalTime momentPornire = LocalTime.now();
    LocalDate dataAzi = dataAziCompleta.toLocalDate();
    Month lunaAzi = dataAziCompleta.getMonth();
    int ziAzi = dataAziCompleta.getDayOfMonth();
    int oraAzi = dataAziCompleta.getHour();
    int minutAzi = dataAziCompleta.getMinute();
    int secundaAzi = dataAziCompleta.getSecond();
    String oraCompleta = oraAzi+":"+minutAzi+":"+secundaAzi;
    int nr_runde;
    
    public void init() {
    numarAleator= (int)(20*Math.random()+1);
    emp = new Label("");
    setLayout(new GridLayout(15,1));
    setBackground(Color.ORANGE);
    setSize(600,600);
    
    System.out.println("Data completa pentru azi: "+dataAziCompleta);
    System.out.println("Data azi: "+dataAzi);
    System.out.println("Luna curenta: "+lunaAzi+" Ziua curenta: "+ziAzi);
    System.out.println("Ora exacta... calculata de noi :): "+oraAzi+":"+minutAzi+":"+secundaAzi);    
    System.out.println("Ora exacta din clasa LocalTime: "+momentPornire);    
    
    label1 = new Label("Ghiciti numarul la loteria noastra");
    label1.setFont(fontTip1);
    label1.setForeground(Color.BLUE);
    label1.setAlignment(label1.CENTER);
    add(label1);
    
    label2 = new Label("Trebuie sa ghiciti un numar intre 1 si 20 - aveti 5 incercari");
    label2.setAlignment(label2.CENTER);
    add(label2);
    
    intrare = new TextField(5);
    intrare.setFont(fontTip2);
    add(intrare);
    
    label3 = new Label("");
    label3.setAlignment(label3.CENTER);
    add(label3);
    label4 = new Label("");
    label4.setAlignment(label4.CENTER);
    label4.setFont(fontTip1);
    label4.setForeground(Color.MAGENTA);
    add(label4);
    
    label5 = new Label("");
    label5.setAlignment(label5.CENTER);
    add(label5);
    
    label6 = new Label("");
    label6.setAlignment(label6.CENTER);
    add(label6);
        
    label7 = new Label("Ora de start: "+oraCompleta);
    label7.setAlignment(label7.CENTER);
    add(label7);
    
    continuare = new Button("Joc nou");
    continuare.setEnabled(false);
    continuare.setFont(fontTip2);
    add(continuare);
    
    label8 = new Label("");
    label8.setAlignment(label8.CENTER);
    label8.setFont(fontTip1);
    add(label8);
    intrare.addActionListener(this);
    continuare.addActionListener(this);
    label9 = new Label("");
    add(label9);
    label10 = new Label("");
    add(label10);
    }
    
    public void numarIncercari() {
    if (incercari>1) {
        label6.setText("Mai ai "+ --incercari + " incercari.");
        label3.setText("Incearca alt numar: ");
        label6.setFont(fontTip1);
                     }
    else {
    label6.setText("Ooops... gata, s-a terminat!");
    momentOprire = LocalTime.now();
    long durataOre = ChronoUnit.HOURS.between(momentx, momentOprire);
    long durataMinute = ChronoUnit.MINUTES.between(momentx, momentOprire);
    long durataSecunde = ChronoUnit.SECONDS.between(momentx, momentOprire);
    System.out.println("Timp in jocul curent : " + durataOre+":"+ durataMinute+":"+ durataSecunde);
    label10.setText("Timp in jocul curent : " + durataOre+":"+ durataMinute+":"+ durataSecunde);
    System.out.println("Timp in jocul curent : " + durataOre+":"+ durataMinute+":"+ durataSecunde);
    label8.setText("Numarul castigator la loto era: "+numarAleator);
    label6.setFont(fontTip1);
    intrare.setEnabled(false);
    continuare.setEnabled(true);
    continuare.requestFocus();
    repaint();
         }
    }
    
    public void clear() {
        intrare.setText("");
    }
    
    public void paint(Graphics g) {
        if (numarAleator==numarPropus && incercari>1) {
            g.setFont(fontTip3);

            g.setColor(new Color(150,55,25));
            g.drawString("Ai CASTIGAT!",170,520);
            g.drawString("...experienta",170,570);
            momentOprire = LocalTime.now();
            long durataOre = ChronoUnit.HOURS.between(momentx, momentOprire);
            long durataMinute = ChronoUnit.MINUTES.between(momentx, momentOprire);
            long durataSecunde = ChronoUnit.SECONDS.between(momentx, momentOprire);
            label10.setText("Timp in jocul curent : " + durataOre+":"+ durataMinute+":"+ durataSecunde);       
            System.out.println(label10.getText());
                                                       }   
        else if (incercari==1) {
                g.setFont(fontTip3);
                g.setColor(new Color(150,55,25));
                g.drawString("S-a terminat!", 130, 400);
                    }
             else {
                g.setFont(fontTip3);
                g.setColor(new Color(150,55,25));
                g.drawString("Start joc",(getSize().width-fm.stringWidth("Start joc"))/2,520);
                  }
    }
    
    public void actionPerformed(ActionEvent e){
    showStatus("Super-aplicatie ca exemplu didactic :)");
    numarPropus = Integer.parseInt(intrare.getText());
    if (e.getSource()==continuare) {
        i=0;
        numarAleator = (int)(20*Math.random()+1);
        continuare.setEnabled(false);
        intrare.setEnabled(true);
        //label3.setText("");
        label4.setText("");
        label5.setText("");
        label6.setText("");
        label7.setText("");
        label8.setText("");
        label9.setText("");
        label10.setText("");
        incercari = 5;
        clear();
        repaint();
        }                                      
    if(e.getSource()==intrare) {
        i++;         
        if(i==1){    
            momentx=LocalTime.now(); 
            LocalTime momentOprire = LocalTime.now();
            label9.setText("Timp petrecut pana la inceperea jocului : "+ChronoUnit.HOURS.between(momentPornire, momentOprire) +":"+ChronoUnit.MINUTES.between(momentPornire, momentOprire)+":"+ChronoUnit.SECONDS.between(momentPornire, momentOprire));
            
        }
        if (numarAleator>numarPropus) { 
                label4.setText("Introduceti un numar mai mare decat " + numarPropus + " !");
                numarIncercari();
                intrare.requestFocus();
                intrare.selectAll();
                                      }               
        else if (numarAleator<numarPropus) {
                    label4.setText("Introduceti un numar mai mic decat " + numarPropus + " !");
                    numarIncercari();
                    intrare.requestFocus();
                    intrare.selectAll();                                            }
             else {
            
            LocalDateTime dataFinalCompleta = LocalDateTime.now();
            LocalTime momentOprir = LocalTime.now();
            int oraFinal = dataFinalCompleta.getHour();
            int minutFinal = dataFinalCompleta.getMinute();
            int secundaFinal = dataFinalCompleta.getSecond();
            String oraFinalCompleta = oraFinal+":"+minutFinal+":"+secundaFinal;        
            long durataOre = ChronoUnit.HOURS.between(momentPornire, momentOprir);
            long durataMinute = ChronoUnit.MINUTES.between(momentPornire, momentOprir);
            long durataSecunde = ChronoUnit.SECONDS.between(momentPornire, momentOprir);
            long durataMilisecunde = ChronoUnit.MILLIS.between(momentPornire, momentOprir);
            long durataNanosecunde = ChronoUnit.NANOS.between(momentPornire, momentOprir);
            
                    label4.setText("Ai nimerit! Felicitari si fugi sa joci la loto!");
                    label3.setText("");
                    //label5.setText("");
                    label5.setText("Ora de final: "+oraFinalCompleta+".   Timp total: "+durataOre+":"+durataMinute+":"+durataSecunde+"."+durataMilisecunde+"."+durataNanosecunde);
                    //label6.setText("");
                    label7.setText("");
                    intrare.setEnabled(false);
                    continuare.setEnabled(true);
                    incercari = 5;
                    repaint();
                   }
        }
    }
//    public void start(){
//        if(nume==null)
//        label3.setText("Introduceti un nume de utilizator");
//        else
//        label3.setText("Introduceti un nr de runde");
        
//    }
}