import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class T23 extends Applet{
    int x=30,y=30, d=12;
    Color c=Color.BLACK;
    Color fundal=Color.YELLOW;
    
    @Override
    public void update(Graphics g){
        g.setColor(c);
        setBackground(fundal);
        g.fillOval(x, y, d, d);
        showStatus("Culoarea curenta RGB: "+c+" Grosimea tusei: "+d);
    }


    public void paint (Graphics g){
        g.drawString("Culori disponibile: r(rosu), g(verde), b (albastru), y(galben), o(portocaliu), p(roz), e(alb)",10,10);
    }

    
    @Override
    public void init(){
        setSize(900,900);
//        setBackground(Color.WHITE);
//        setBackground(fundal);
        try{
        FileWriter fw = new FileWriter("output.txt", true);
        BufferedWriter bw = new BufferedWriter(fw);
        
        class tML implements MouseListener,MouseMotionListener,KeyListener{
            @Override
            public void mouseClicked(MouseEvent e){
                if (e.getButton()==MouseEvent.BUTTON1){
                    d+=4;
                    try{
                    bw.write("Click stanga ->Marire tusa la "+d+"\n");
                    bw.flush();
                    }catch(IOException e1){};
                }
                else if (d>4) {
                    d-=4;
                    try{
                    bw.write("Click stanga ->Micsorare tusa la "+d+"\n");
                    bw.flush();
                    }catch(IOException e1){};
                }
            showStatus("Culoarea curenta RGB: "+c+" Grosime tusa: "+d+"\n");
            }
            
            @Override
            public void mouseReleased(MouseEvent e){}
            @Override
            public void mousePressed(MouseEvent e){}
            @Override
            public void mouseEntered(MouseEvent e){}
            @Override
            public void mouseExited(MouseEvent e){}
            @Override
            public void mouseMoved(MouseEvent e){}
            @Override
            public void mouseDragged(MouseEvent e){
                x=e.getX()-d/2;
                y=e.getY()-d/2;
                repaint();
            }
            public void keyPressed(KeyEvent e){}
            public void keyReleased(KeyEvent e){}
            public void keyTyped(KeyEvent e){
                char ch=e.getKeyChar();
                switch (ch){
                    case 'r':try{c=Color.RED;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"rosu"+"\n");
                    bw.flush();
                    }catch(IOException e1){}; break;
                    case 'y': try{c=Color.YELLOW;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"galben"+"\n");
                    bw.flush();
                    }catch(IOException e1){};break;
                    case 'b': try{c=Color.BLUE;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"albastru" +" si fundal cu rosu"+"\n");
                    bw.flush();
                    }catch(IOException e1){};break;
                    case 'g': try{c=Color.GREEN;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"verde"+"\n");
                    bw.flush();
                    }catch(IOException e1){};;break;
                    case 'o': try{c=Color.ORANGE;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"portocaliu"+"\n");
                    bw.flush();
                    }catch(IOException e1){};break;
                    case 'p': try{c=Color.PINK;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"roz"+"\n");
                    bw.flush();
                    }catch(IOException e1){};break;
                    case 'e': try{c=Color.WHITE;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"alb"+"\n");
                    bw.flush();
                    }catch(IOException e1){};break;
                    case 'f': try{c=Color.RED;fundal=Color.PINK;
                    bw.write("Apasare tasta "+ch+" ->"+"tusa cu rosu si"+" fundal roz"+"\n");
                    bw.flush();
                    }catch(IOException e1){};repaint();break;
                    default :try{c=Color.BLACK;
                    bw.write("Apasare tasta "+ch+" ->tusa cu "+"negru"+"\n");
                    bw.flush();
                    }catch(IOException e1){};
                }
        showStatus("Culoarea curenta RGB: "+c+" Grosime tusa: "+d);                                   
            }
        }
        
        tML m=new tML();
        addMouseListener(m);
        addMouseMotionListener(m);
        addKeyListener(m);
        setFocusable(true);
        requestFocus();
    }
    catch(IOException e){};
    }
}