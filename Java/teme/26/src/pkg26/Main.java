package pkg26;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
 
         
public class Main extends Application {  
   
   @Override 
   public void start(Stage stage) throws Exception { 
       
      //pregatirea obiectelor ObservableList 
       
       Scanner a = new Scanner(System.in);
       Scanner b = new Scanner(System.in);
      ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
         new PieChart.Data("Iphone xx", 20), 
         new PieChart.Data("Samsung Galaxy Sxx", 30), 
         new PieChart.Data("Huawey Pxx", 10), 
         new PieChart.Data("LG", 13),
         new PieChart.Data("QinLinTin", 10),
         new PieChart.Data("Nokia", 5));
//          new PieChart.Data(a.nextLine(), b.nextInt()),
//          new PieChart.Data(a.nextLine(), b.nextInt()), 
//          new PieChart.Data(a.nextLine(), b.nextInt()), 
//          new PieChart.Data(a.nextLine(), b.nextInt()), 
//          new PieChart.Data(a.nextLine(), b.nextInt()), 
//          new PieChart.Data(a.nextLine(), b.nextInt()), 
//          new PieChart.Data(a.nextLine(), b.nextInt()));
      //create piechart
      PieChart pieChart = new PieChart(pieChartData); 
              
      //titlul pie chart-ului 
      pieChart.setTitle("Vanzari telefoane mobile in 20xx"); 
       
      //directia in care se pun datele in chart 
      pieChart.setClockwise(true); 
       
      //lungimea liniei de conectare intre marca si felia de chart 
      pieChart.setLabelLineLength(30); 

      //setarea vizibilitatii etichetelor chart-ului  
      pieChart.setLabelsVisible(true); 
       
      //unghiul de start al chartului  
      pieChart.setStartAngle(90);
//      pieChart.setOnMouseEntered(e->{
//         String x =pieChart.Data.getData();
//      });      
      Group root = new Group(pieChart); 
        final Label caption = new Label("");
        caption.setTextFill(Color.BLACK);
        caption.setStyle("-fx-font: 20 arial;");
      for (final PieChart.Data data : pieChart.getData()) {
        data.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                caption.setTranslateX(e.getSceneX()+10);
                caption.setTranslateY(e.getSceneY()-10);
                caption.setText(String.valueOf(data.getPieValue()));
            }
        });
      }
      //crearea obiectului radacina
      Button but = new Button();
      but.setTranslateX(30);
      but.setTranslateY(30);
      but.setText("EXIT");
      but.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent e) {
          try {
              FileWriter fw = new FileWriter("file.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                for (final PieChart.Data data : pieChart.getData()){
                bw.write(data.getName()+" "+data.getPieValue()+"\n");
                bw.flush();
                }
          } catch (IOException ex) {}
        
        System.exit(1);
    }
});
      root.getChildren().add(caption);
      root.getChildren().add(but);
      //crearea scenei
      Scene scene = new Scene(root, 600, 400);  
      
      //titlul ferestrei Stage
      stage.setTitle("Statistica dulce... placinta :)"); 
         
      //adaugarea scenei la stage 
      stage.setScene(scene); 
         
      //afisarea continutului Stage-ului 
      stage.show();         
   }     
   public static void main(String args[]){ 
      launch(args); 
   } 
}