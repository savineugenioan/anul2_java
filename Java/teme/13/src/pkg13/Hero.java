package pkg13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author STT2
 */
public class Hero {
    protected String name;
    protected int health = 100;
    protected int damage;
    protected int bonus =0;
    static int morti=0;
    Hero tinta;
    
    static final double PROBABILITYOFATTACK = 0.8;
    public void Afisare()
    {
        System.out.print("Name : "+this.name);
        System.out.print(" Viata : "+this.health);
        System.out.print(" Forta : "+this.damage);
        System.out.println("");
    }
    
    public void attack(Hero victim){
        if(!(this.health>0 && victim.health>0)){
            //System.out.println("Cannot attack, not both heroes are alive");
            return;
        }
        int dmg=this.damage+bonus;  
        System.out.println(this.name+" il/o ataca pe "+victim.name+" cu forta : "+dmg + " (bonus "+this.bonus+")");
        this.bonus=0;
        victim.health-=dmg;
        int bonus = (int)(Math.random()*5)+5;
        if(dmg>10 && dmg<15)
            victim.bonus=bonus;
        if(victim.health<=0)
        {
            victim.health = 0;
            System.out.println(victim.name+" a fost ucis/ucisa");
            morti++;
        }
    }
       
    public static List<Hero> CreareLista(Hero... eroi){
        List<Hero> Eroi= new ArrayList<>();
        Eroi.addAll(Arrays.asList(eroi));
        return Eroi;
    }    
    public static void setTinta(List<Hero> Eroi){
           List<Hero> Eroi_1= new ArrayList<>(Eroi);
           //Collections.copy(Eroi_1, Eroi);
           Random q = new Random();
           while(true){
            int i = q.nextInt(Eroi_1.size());
            int j;
            while((j = q.nextInt(Eroi_1.size())) == i);
            //int j = q.nextInt(Eroi_1.size());
            Eroi_1.get(i).tinta=(Eroi_1.get(j));//Eroi_1.get(j).tinta=(Eroi.get(i));
            if(i>j)
            {Eroi_1.remove(i);Eroi_1.remove(j);}
            else
            {Eroi_1.remove(j);Eroi_1.remove(i);}
            if(Eroi_1.isEmpty())
                break;
        }
       }
    public Hero(String name,int damage){
        this(name,damage,100);
    }
    public Hero(Hero x){
        this(x.name,x.damage,x.health);
    }
    private Hero(String name,int damage,int health){
        super();
    this.name=name;
    this.damage=damage;
    this.health=health;
}
}
