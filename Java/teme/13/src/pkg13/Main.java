/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg13;

import java.util.*;
import java.util.Random;

/**
 *
 * @author EUGEN-IOANSAVIN
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hero a,b,c,d;
        int runda = 0,morti=0,n;
        Random q = new Random();
        a=new Hero("Gigi",10+q.nextInt(11));b=new Hero("Ana",10+q.nextInt(11));
        c=new Hero("Vasile",10+q.nextInt(11));d=new Hero("Maria",10+q.nextInt(11));
        List<Hero> Eroi = Hero.CreareLista(a,b,c,d);
        Hero.setTinta(Eroi);
        n=Eroi.size()/2;
        while(Hero.morti<n){
            runda++;
            System.out.println("Runda "+ runda);
            if(runda%2==1){
                for(Hero h : Eroi)
                    if(h.tinta!=null){
                        h.attack(h.tinta);    
                    }
            }
            else{
                for(Hero h : Eroi)
                    if(h.tinta!=null){
                        h.tinta.attack(h);
                    }
            }   
            System.out.println("");
            for(Hero h : Eroi)
                h.Afisare();
            System.out.println("_______________________");
        }
        System.out.println("Final:");
        for(Hero h : Eroi)
            h.Afisare();
    }
    
}
