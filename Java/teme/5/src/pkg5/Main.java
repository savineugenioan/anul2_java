/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg5;
import java.util.Scanner;
/**
 *
 * @author EUGEN-IOANSAVIN
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int x,y;char o;
        Scanner a = new Scanner(System.in);
        boolean bol;
        String str ;
        try{
            do{
                bol=false;
                System.out.print("operatie = ");
                o = a.nextLine().charAt(0);
                System.out.print("nr1 = ");
                x=Integer.parseInt(a.nextLine());
                System.out.print("nr2 = ");
                y=Integer.parseInt(a.nextLine());
                switch(o){
                    case'+': System.out.println(x+y);break;
                    case '-': System.out.println(x-y);break;
                    case '*': System.out.println(x*y);break;
                    case '/': System.out.println(x/y);break;
                    default: System.out.println("Operator gresit");break;
                }
                System.out.println("Doriti continuarea procedurii ?\n Scrieti 'DA' pentru a continua, sau apasati Enter");
                str = a.nextLine();
                if("DA".equals(str))
                    bol=true;
            }while(bol);
        }
        catch(NumberFormatException e){
            System.out.println("Nu toate numerele sunt intragi");
        }
        catch(ArithmeticException e){
            System.out.println("Nu se poate imparti la 0");
        }
            
    }
    
}
