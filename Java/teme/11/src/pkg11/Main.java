/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author EUGEN-IOANSAVIN
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static int calcul(int nr_var){
        char[] semne = {'+','-'};
        int[] v = new int[nr_var];
        Random aleator = new Random();
        int i=0,rez = 0;
        while(i<nr_var){
                    v[i]= aleator.nextInt(1000);// pentru a nu fi calcule ridicol de mari
                            i++;
                }
        i=0;
        rez=v[0];
        System.out.print(v[0]);
        while(i<nr_var-1){
            char o =semne[aleator.nextInt(2)];
            System.out.print(o+""+v[i+1]);
            switch(o){
                case '+' : rez +=v[i+1];break;
                case '-' : rez -=v[i+1];break;
            }
            i++;
            }
        System.out.print("=");
        return rez;
    }
    public static void main(String[] args) {
        try{
            int raspuns_user;
            int contor_gresit = 0,contor = 0;
            int lungime;
            Scanner a = new Scanner(System.in);   
            System.out.print("Nume jucator : ");
            String nume = a.nextLine();
            System.out.print("Introduceti numarul de operatii ( de la 3 la 10 ) : ");
            try{
            lungime = Integer.parseInt(a.nextLine());
            if( lungime<3 || lungime >10)
            {System.out.println("Numar de operatii introdus gresit. Se vor efectua 10 operatii.");lungime = 10;}
            }
            catch(Exception e){
                System.out.println("Numar de operatii introdus gresit. Se vor efectua 10 operatii.");lungime = 10;
            }
            for(int i=0;i<lungime;i++){
                Random aleator = new Random();
                int nr_var = 2+aleator.nextInt(4);
                int raspuns = calcul(nr_var); 
                try{
                raspuns_user = Integer.parseInt(a.nextLine());}
                catch(Exception e){
                    contor_gresit++;
                    System.out.println("Raspuns gresit !");contor_gresit++;
                    continue;
                }
                if(raspuns_user==raspuns)
                {System.out.println("Raspuns corect !");contor++;}
                else{
                    System.out.println("Raspuns gresit !");contor_gresit++;
                }
            }
            System.out.println("Scorul lui "+ nume +" este : "+ contor);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
    
}
