
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author STT2
 */
public class SnakeRawVersion extends JFrame implements Runnable{
    int snakeX,snakeY,snakeD=12,appleX,appleY,appleD=4;
    int xDir,yDir;
    int score=0;
    JPanel drawArea = new JPanel(){
        public void paintComponent(Graphics g){
            super.paintComponent(g);
            g.fillOval(snakeX, snakeY, snakeD, snakeD);
            g.fillOval(appleX, appleY, appleD, appleD);
            g.drawString("scor "+score, getWidth()-50, getHeight()-10);
        }  
    };
            
    public SnakeRawVersion()
    {
        super("snake");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,400);
        setLayout(new GridLayout(1,1));
        add(drawArea);
        
        addKeyListener(new KeyAdapter(){
        public void keyPressed(KeyEvent e){
            int code=e.getKeyCode();
            switch(code){
                case KeyEvent.VK_LEFT: xDir=-1;yDir=0;break;
                case KeyEvent.VK_RIGHT: xDir=1;yDir=0;break;
                case KeyEvent.VK_UP: xDir=0;yDir=-1;break;
                case KeyEvent.VK_DOWN: xDir=0;yDir=1;break;

            }
        }
    });
        
        setVisible(true);
        appleX=(int)(Math.random()*(getWidth()-appleX));
        appleX=(int)(Math.random()*(getWidth()-appleX));
        drawArea.repaint();
    }
    public void run(){
        while(true){
            snakeX+=xDir;
            snakeY+=yDir;
            if(Math.sqrt(Math.pow(snakeX+snakeD/2-appleX-appleD/2,2)+Math.pow(snakeY+snakeD/2-appleY-appleD/2,2))<(snakeD+appleD)/2){
                appleX=(int)(Math.random()*(getWidth()-appleD));
                appleX=(int)(Math.random()*(getHeight()-appleD));
                score++;
            }
                
            drawArea.repaint();
            try{
                Thread.sleep(25);
            }
            catch(InterruptedException E){}
        }
    }
    public static void main(String[] args){
        new Thread(new SnakeRawVersion()).start();
}
}
