/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author STT2
 */
public class Hero {
    private String name;
    private int health = 100;
    private int damage;
    private int level = 1;
    public void Afisare()
    {
        System.out.println("Name : "+this.name);
        System.out.println("Health : "+this.getHealth());
        System.out.println("Damage : "+this.getDamage());
        System.out.println("Level : "+this.getLevel());
    }
    
    public void attack(Hero victim){
        if(!(this.health>0 && victim.health>0)){
            System.out.println("Cannot attack, not both heroes are alive");
            return;
        }
        //scriem atacul 
    }

    public Hero(String name,int damage){
        this(name,damage,100,1);
    }
    public Hero(Hero x){
        this(x.name,x.damage,x.health,x.level);
    }
    private Hero(String name,int damage,int health,int level){
    this.name=name;
    this.damage=damage;
    this.health=health;
    this.level=level;
}
    public String getName(){
       return this.name;
    }
    public void setName(String nume){
        this.name = nume;
    }
    public int getHealth() {
        return health;
    }
    public int getDamage() {
        return damage;
    }
    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }  
}
