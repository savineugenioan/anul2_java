package lab_4_joczaruri;

class Jucator {
    int id;
    String nume;
    int scor;
    
    Jucator(int id, String nume) {
        this.id = id;
        this.nume = nume;
        this.scor = 0;
    }
    
    /**
     *  Actualizarea scorului jucatorilor
     */
    public void actualizareScor(int zarCurent) {
        scor += zarCurent;
        System.out.format("\n%s \tscor = %d\n", nume, scor);
    }
    
    @Override
    public String toString() {
        return nume + " [" + scor + "]";
    }
}

class Zar {
    /**
     *  Dam cu zarul si cautam un numar intreg intre 1 si 6
     */
    public int daCuZarul() {
        return 1 + (int)(Math.random() * 6);
    }
}

class Joc {
    Jucator jucator1;
    Jucator jucator2;
    Zar zarDat;
    
    /**
     *  Constructorul obiectelor de tip Joc
     */
    Joc() {
        // instantiem doi jucatori
        jucator1 = new Jucator(1, "Ana");
        jucator2 = new Jucator(2, "Ion");
        
        // instantiem obiectul de tip Zar
        zarDat = new Zar();
    }
    
    /**
     *  Afiseaza castigatorul pe baza scorului
     */
    private Jucator castigator() {
        if(jucator1.scor > jucator2.scor) {
            return jucator1;
        } else if(jucator2.scor > jucator1.scor) {
            return jucator2;
        } else {
            return null;
        }
    }
    
    /**
     *  Se joaca jocul in urmatorii pasi:
     *  1. Arunca zarul
     *  2. Actualizeaza scorul jucatorului curent pe baza zarului de la pasul 1
     *  3. Schimba jucatorul curent
     *  4. Repeta pasii de la 1 pana cand numarRunde devine 0 (se repeta de multe ori).
     *  5. Verifica ce jucator are scor mai mare si afiseaza castigator sau egal.
     */
    public void joaca() {
        Jucator jucatorCurent = jucator1; // incepem cu acest jucator
        int zarCurent;
        int numarRunde = 4;  // numarul de runde pe care le jucam,atentie! un numar impar aduce o runda in plus pentru jucator1
        
        while(numarRunde-- > 0) {
            // da cu zarul
            zarCurent = zarDat.daCuZarul();
            
            // actualizeaza scorul jucatorului curent
            jucatorCurent.actualizareScor(zarCurent);
            
            // schimba jucatorul curent
            if(jucatorCurent == jucator1) {
                jucatorCurent = jucator2;
            } else {
                jucatorCurent = jucator1;
            }
        }
        
        // afiseaza castigator sau egalitate
        if(castigator() != null) {
            System.out.println("\n\n\t"+castigator() + " castiga!\n\n");
        } else {
            System.out.println("S-a terminat la egalitate.");
        }
    }
}

public class Lab_4_JocZaruri {

    public static void main(String[] args) {
        // instantiem un obiect de clasa Joc
        Joc joc = new Joc();
        
        // start game play
        joc.joaca();
    }
    
}
