package lab_3_scrierefisier;

/**
 *
 * @author Alex Tabusca
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Lab_3_ScriereFisier {

 public static void main(String[] args) {
  BufferedReader obiectCititTemporar = null;
  try {
   String liniaCurenta;

   obiectCititTemporar = new BufferedReader(new FileReader("C:\\_Temporar\\JAVA Labs\\fisier3.txt"));

   FileOutputStream fluxIesire = new FileOutputStream("c:\\_Temporar\\JAVA Labs\\fisier3duplicat.txt");
   OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fluxIesire, "UTF-16");
   BufferedWriter scriitorTemporar = new BufferedWriter(outputStreamWriter);
   
   while ((liniaCurenta = obiectCititTemporar.readLine()) != null) {
    System.out.println(liniaCurenta);
    scriitorTemporar.write(liniaCurenta);
    scriitorTemporar.newLine();
   }
   scriitorTemporar.close();   
   
/*
   FileOutputStream outputStream = new FileOutputStream("c:\\_Temporar\\JAVA Labs\\fisier3b.txt");
   OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
   BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
   bufferedWriter.write("Azi e frumos afara!");
   bufferedWriter.newLine();
   bufferedWriter.write("Toata lumea e vesela si bine dispusa :)");
   bufferedWriter.close();   
*/
   
  } catch (IOException e) {
   e.printStackTrace();
  } 

 }

}