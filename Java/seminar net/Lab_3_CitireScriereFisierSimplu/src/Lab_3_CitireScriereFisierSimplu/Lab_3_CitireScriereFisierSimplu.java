package Lab_3_CitireScriereFisierSimplu;

import java.io.*;

class Lab_3_CitireScriereFisierSimplu {
String text1 ="Cioc cioc! de ";
int intreg=20;
String text2=Integer.toString(intreg);

String text3=text1+text2+" ori!";

public void citeste(String fisier) {
 int c;
 FileReader f = null;
  try {
   f = new FileReader(fisier);
   while ((c = f.read()) != -1) {   //read citeste cate un caracter si returneaza codul lui ca int; citind pana la -1
    text1 = text1 + (char)c;
   }
   f.close();
  }
  catch (FileNotFoundException e) {
   System.out.println("Fisierul nu a fost gasit");
  }
  catch (IOException e) {
   System.out.println("Eroare la citire");
  }
}
	
public void scrie(String fisier) {
 FileWriter f = null;
 try {
  f = new FileWriter(fisier);
  f.write(text3);
  f.close();
  }
 catch (IOException e) {
  System.out.println("Eroare la scriere");
 }
}
 
public static void main(String argv[]) {
Lab_3_CitireScriereFisierSimplu obiect = new Lab_3_CitireScriereFisierSimplu();
obiect.citeste("C:\\_Temporar\\fisier.txt");
obiect.scrie("c:\\_Temporar\\iesire.txt");

File fisierGol = new File("c:\\_Temporar\\fisierGol.txt");
    try {
        fisierGol.createNewFile();
    } catch (IOException ex) {
    System.out.print("Ghinion :(");
    }
}

}