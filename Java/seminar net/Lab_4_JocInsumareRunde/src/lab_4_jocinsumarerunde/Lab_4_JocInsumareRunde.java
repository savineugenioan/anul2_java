package lab_4_jocinsumarerunde;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Lab_4_JocInsumareRunde {

    public static void main(String[] args) {
        // TODO code application logic here
        int numarRunde;
        String numeJucator1, numeJucator2;
        Random aleatoriu = new Random();
        int totalJucator1=0, totalJucator2=0;
        
        
        System.out.print("Cate runde jucati? (1-5) ");
        Scanner rundeCitite = new Scanner(System.in);
        
        try {
        numarRunde = rundeCitite.nextInt(6);
        }
        catch (InputMismatchException eroare) {
            System.out.println("Daca nu va hotarati la un NUMAR intre 1 si 5... o sa jucati 5.");
            numarRunde=5;
        }
            
        System.out.println("OK - jucam "+numarRunde+" runde.");
               
        System.out.print("Jucator 1: ");
        Scanner numeCitit1 = new Scanner(System.in);
        numeJucator1 = numeCitit1.nextLine();
        
        System.out.print("Jucator 2: ");
        Scanner numeCitit2 = new Scanner(System.in);
        numeJucator2 = numeCitit2.nextLine();
        
        System.out.println("\n\tSa recapitulam:\n\t"+numeJucator1+" si "+numeJucator2+" se joaca timp de "+numarRunde+" runde.\n");
        
        for (int i=0; i<numarRunde; i++)
        {
            int zarTemporar1=aleatoriu.nextInt(7);
            System.out.println("\n"+numeJucator1+" a dat in runda "+(i+1)+" zarul "+zarTemporar1);
            totalJucator1+=zarTemporar1;
            
            int zarTemporar2=aleatoriu.nextInt(7);
            System.out.println(numeJucator2+" a dat in runda "+(i+1)+" zarul "+zarTemporar2);
            totalJucator2+=zarTemporar2;
        }
        
        System.out.println("\n\tTotalul pentru "+numeJucator1+" este: "+totalJucator1);
        System.out.println("\tTotalul pentru "+numeJucator2+" este: "+totalJucator2+"\n");
        
        //int diferentaScor=totalJucator1-totalJucator2;
        
        switch (totalJucator1-totalJucator2)
        {
            case -1: System.out.println(numeJucator1+"a castigat la mustata!");break;
            case 0: System.out.println("Egalitate");break;
            case 1: System.out.println(numeJucator1+"a castigat la mustata!");break;
            default: if (totalJucator1-totalJucator2>=2) {System.out.println("A castigat "+numeJucator1);} else {System.out.println("A castigat "+numeJucator2);}
        }
        
    }
    
}