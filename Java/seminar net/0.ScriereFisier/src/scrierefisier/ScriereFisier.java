package scrierefisier;

/**
 *
 * @author Alex Tabusca
 */

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

 
public class ScriereFisier {
 
    public static void main(String[] args) {
        try {
            FileOutputStream fluxIesire = new FileOutputStream("c:\\_Temporar\\JAVA Labs\\fisier2.txt");
//            FileOutputStream fluxIesire = new FileOutputStream("c:\\fisier2.txt");
            OutputStreamWriter scriitorDinFlux = new OutputStreamWriter(fluxIesire, "UTF-16");
            BufferedWriter scriitorTemporar = new BufferedWriter(scriitorDinFlux);
             
            scriitorTemporar.write("Azi e o zi frumoasa.");
            scriitorTemporar.newLine();
            scriitorTemporar.write("Ana are mere.");
            scriitorTemporar.newLine();
            scriitorTemporar.write("Ion are si el mere.");
            scriitorTemporar.write(" Ana \u2764 Ion");
            scriitorTemporar.close();
        }
        catch (IOException e) {
//            System.out.println("Eroare!");
            e.printStackTrace();
        }
    }
}