package lab_2_citireconsola2_basic;

import java.util.InputMismatchException;
import java.util.Scanner;
 
public class Lab_2_CitireConsola2_basic{
   public static void main(String[] args) 
   {
      Scanner sc1 = new Scanner(System.in);  


      System.out.print("Introduceti un nume: ");
      String sir = sc1.nextLine();
      System.out.println("-----------------");
      
      System.out.print("Introduceti varsta: ");
      int varsta = sc1.nextInt();
      System.out.println("-----------------");
      
      System.out.print("Introduceti salariul: ");
      double salariu = sc1.nextDouble();
      System.out.println("-----------------");
      
      // sa vedem ce am introdus
      System.out.println("Persoana: "+sir);
      System.out.println("Varsta: "+varsta);
      System.out.println("Salariul: "+salariu);
   }
}