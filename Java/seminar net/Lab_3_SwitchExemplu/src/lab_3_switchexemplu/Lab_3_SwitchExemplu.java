package lab_3_switchexemplu;

import java.util.Random;

public class Lab_3_SwitchExemplu {    
public static void main(String[] argumente) {    
    
//    int luna=7;    
    Random aleatoriu = new Random();
    double aleatoriu2 = Math.random(); //un numar aleatoriu intre 0 inclusiv si 1 exclusiv
    
    System.out.println("Numarul aleatoriu generat ca obiect Random este: "+aleatoriu);
    System.out.println("Numarul aleatoriu generat ca double: "+aleatoriu2+"\n");    
    
    float aleatoriuFloat = aleatoriu.nextFloat(); 
    System.out.println("Numarul aleatoriu generat ca numar in virgula mobila \"float\" este: "+aleatoriuFloat);
    
    int luna = aleatoriu.nextInt(12);  //NU are legatura de fapt cu continutul obiectului "aleatoriu" de tip Random
    System.out.println("Numarul aleatoriu generat ca numar intreg \"int\" este: "+luna);    
    
    String lunaSir="";  

    switch(luna){
    case 1: lunaSir="Luna 1 - Ianuarie"; break;   
    case 2: lunaSir="Luna 2 - Februarie"; break;
    case 3: lunaSir="Luna 3 - Martie"; break; 
    case 4: lunaSir="Luna 4 - Aprilie"; break;
    case 5: lunaSir="Luna 5 - Mai"; break;
    case 6: lunaSir="Luna 6 - Iunie"; break;
    case 7: lunaSir="Luna 7 - Iulie"; break;
    case 8: lunaSir="Luna 8 - August"; break;
    case 9: lunaSir="Luna 9 - Septembrie"; break;
    case 10: lunaSir="Luna 10 - Octombrie"; break;
    case 11: lunaSir="Luna 11 - Noiembrie"; break;
    case 12: lunaSir="Luna 12 - Deecmbrie"; break;
    default:System.out.println("Asta nu luna valida!");
    }

    System.out.println(lunaSir);  
}    
} 