package lab_3_ifcomplex;

public class Lab_3_IfComplex {  
public static void main(String[] argumente) {  
    String s;

//  int nota=6;  
    int nota;
    
    try {
    nota = Integer.parseInt(argumente[0]);
    System.out.println("Nota a fost preluata ca parametru din linia de comanda");
    System.out.println("Nota dumneavoastra este: "+nota+"\n------------------------");
    }
    catch (ArrayIndexOutOfBoundsException eroare) {
        System.out.println("NU e niciun parametru in linia de comanda... primiti din oficiu 4!");
        nota = 4;
    }

    if(nota<5){  
        System.out.println("picat!");  
    }  
    else if(nota>=5 && nota<6){  
        System.out.println("La limita...");  
    }  
    else if(nota>=6 && nota<7){  
        System.out.println("Acceptabil.");  
    }  
    else if(nota>=7 && nota<8){  
        System.out.println("Bine.");  
    }  
    else if(nota>=8 && nota<9){  
        System.out.println("Foarte bine.");  
    }else if(nota>=9 && nota<=10){  
        System.out.println("Excelent!");  
    }else{  
        System.out.println("Nota imposibila!");  
    }  
}  
}