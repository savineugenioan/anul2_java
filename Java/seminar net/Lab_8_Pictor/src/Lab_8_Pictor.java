import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

public class Lab_8_Pictor extends Applet{
    int x=30,y=30, d=12;
    Color c=Color.BLACK;
    Color fundal=Color.YELLOW;
    
    @Override
    public void update(Graphics g){
        g.setColor(c);
        setBackground(fundal);
        g.fillOval(x, y, d, d);
        showStatus("Culoarea curenta RGB: "+c+" Grosimea tusei: "+d);
    }


    public void paint (Graphics g){
        g.drawString("Culori disponibile: r(rosu), g(verde), b (albastru), y(galben), o(portocaliu), p(roz), e(alb)",10,10);
    }

    
    @Override
    public void init(){
        setSize(900,900);
//        setBackground(Color.WHITE);
//        setBackground(fundal);
        class tML implements MouseListener,MouseMotionListener,KeyListener{
            @Override
            public void mouseClicked(MouseEvent e){
                if (e.getButton()==MouseEvent.BUTTON1)
                    d+=4;
                else if (d>4) d-=4;
            showStatus("Culoarea curenta RGB: "+c+" Grosime tusa: "+d);
            }
            
            @Override
            public void mouseReleased(MouseEvent e){}
            @Override
            public void mousePressed(MouseEvent e){}
            @Override
            public void mouseEntered(MouseEvent e){}
            @Override
            public void mouseExited(MouseEvent e){}
            @Override
            public void mouseMoved(MouseEvent e){}
            @Override
            public void mouseDragged(MouseEvent e){
                x=e.getX()-d/2;
                y=e.getY()-d/2;
                repaint();
            }
            public void keyPressed(KeyEvent e){}
            public void keyReleased(KeyEvent e){}
            public void keyTyped(KeyEvent e){
                char ch=e.getKeyChar();
                switch (ch){
                    case 'r': c=Color.RED;break;
                    case 'y': c=Color.YELLOW;break;
                    case 'b': c=Color.BLUE;break;
                    case 'g': c=Color.GREEN;break;
                    case 'o': c=Color.ORANGE;break;
                    case 'p': c=Color.PINK;break;
                    case 'e': c=Color.WHITE;break;
                    case 'f': fundal=Color.PINK;repaint();break;
                    default : c=Color.BLACK;
                }
        showStatus("Culoarea curenta RGB: "+c+" Grosime tusa: "+d);                                   
            }
        }
        
        tML m=new tML();
        addMouseListener(m);
        addMouseMotionListener(m);
        addKeyListener(m);
        setFocusable(true);
        requestFocus();
    }
}