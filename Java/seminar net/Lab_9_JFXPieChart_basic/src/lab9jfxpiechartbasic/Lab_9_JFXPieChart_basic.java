package lab9jfxpiechartbasic;

import javafx.application.Application; 
import javafx.collections.FXCollections;  
import javafx.collections.ObservableList; 
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.stage.Stage; 
import javafx.scene.chart.PieChart; 
         
public class Lab_9_JFXPieChart_basic extends Application {  
   
   @Override 
   public void start(Stage stage) { 
       
      //pregatirea obiectelor ObservableList         
      ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
         new PieChart.Data("Iphone xx", 20), 
         new PieChart.Data("Samsung Galaxy Sxx", 30), 
         new PieChart.Data("Huawey Pxx", 10), 
         new PieChart.Data("LG", 13),
         new PieChart.Data("QinLinTin", 10),
         new PieChart.Data("Nokia", 5));
       
      //create piechart
      PieChart pieChart = new PieChart(pieChartData); 
              
      //titlul pie chart-ului 
      pieChart.setTitle("Vanzari telefoane mobile in 20xx"); 
       
      //directia in care se pun datele in chart 
      pieChart.setClockwise(true); 
       
      //lungimea liniei de conectare intre marca si felia de chart 
      pieChart.setLabelLineLength(30); 

      //setarea vizibilitatii etichetelor chart-ului  
      pieChart.setLabelsVisible(true); 
       
      //unghiul de start al chartului  
      pieChart.setStartAngle(90);     
         
      //crearea obiectului radacina
      Group root = new Group(pieChart); 
         
      //crearea scenei
      Scene scene = new Scene(root, 600, 400);  
      
      //titlul ferestrei Stage
      stage.setTitle("Statistica dulce... placinta :)"); 
         
      //adaugarea scenei la stage 
      stage.setScene(scene); 
         
      //afisarea continutului Stage-ului 
      stage.show();         
   }     
   public static void main(String args[]){ 
      launch(args); 
   } 
}