package lab_1_bunalume_final;

class Lab_1_BunaLume_final
{
    public static void main(String[] arg)
    {
        System.out.println("Buna. Lume!"); // Afisare sir.
                
        // vrem sa afisam mai multe cuvinte primite ca argumente din linia de comanda

/**        
        try { 
            for (int i=0; i<arg.length; i++) System.out.println(arg[i]); //poate da o exceptie daca nu exista argument
            }
        catch (ArrayIndexOutOfBoundsException exceptie)
            {
                System.out.println("..ceva nu e bine - s-a 'prins' ceva eronat...");
            }
*/
        int suma=0; 
        int produs=1;
        try { 
            for (int i=0; i<arg.length; i++) 
             {System.out.println(arg[i]); //poate da o exceptie daca nu exista argument
              suma+=Integer.parseInt(arg[i]);
              produs*=Integer.parseInt(arg[i]);
             }
            }
        catch (ArrayIndexOutOfBoundsException exceptie)
            {
                System.out.println("..ceva nu e bine - s-a 'prins' ceva eronat...");
            }        
        
        System.out.println("Suma argumentelor este: "+suma);
        System.out.println("Produsul argumentelor este: "+produs);

    }
}