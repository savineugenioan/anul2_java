/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_5_starwars_final;

/**
 *
 * @author Alex Tabusca
 */
public interface setMetodeExterne {
//interfata cu metode definite prin default, introduse in Java8. Inainte definitiile de metode erau void si fara cod sursa    
    default void metoda3(){
       System.out.println("metoda3() din interfata setMetode2");
    }
    default void metoda4(){
       System.out.println("metoda4() din interfata setMetode2");
    }
}
