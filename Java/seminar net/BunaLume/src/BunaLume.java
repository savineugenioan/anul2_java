class BunaLume 
{
    public static void main(String[] arg)
    {
        System.out.println("Buna. Lume!"); // Afisare sir.
                
        // vrem sa afisam minim doua cuvinte
        
        try { 
            for (int i=0; i<arg.length; i++) System.out.println(arg[i]); //poate da o exceptie daca nu exista argument
            }
        catch (ArrayIndexOutOfBoundsException exceptie)
            {
                System.out.println("..ceva nu e bine - s-a 'catch' ceva gresit...");
            }
        
        
    }
}