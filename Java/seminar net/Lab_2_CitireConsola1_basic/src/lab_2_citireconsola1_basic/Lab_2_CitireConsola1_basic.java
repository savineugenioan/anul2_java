package lab_2_citireconsola1_basic;

import java.util.Scanner;
 
public class Lab_2_CitireConsola1_basic{
   public static void main(String[] args) 
   {
      // folosind clasa Scanner
      Scanner sc = new Scanner(System.in);  

      System.out.print("Introduceti un text... care sa fie de fapt un numar: ");
      String sir = sc.nextLine();
      
      /*
      int n      = sc.nextInt();        // citeste inputul ca integer
      long k     = sc.nextLong();       // citeste inputul ca long
      double d   = sc.nextDouble();     // citeste inputul ca double
      String s   = sc.nextLine();       // citeste inputul ca String
      ...mai exista si nextBoolean(), nextByte(), nextFloat(), nextShort()
      */      
      
      
      System.out.println("-----------------");
      System.out.println("Ati introdus sirul: \n"+sir);
      
      try {
      int numar=Integer.parseInt(sir);
      System.out.println("Numarul introdus este: "+numar);
      }
      catch (NumberFormatException exceptie) {
      System.out.println("..si doar v-am zis sa introduceti un numar!");
      }

//      catch (NumberFormatException|ArrayIndexOutOfBoundsException exceptie) {
//      System.out.println("..si doar v-am zis sa introduceti un numar!");
//      }
      
//      catch (Exception exceptie) {
//      System.out.println("..asta se printeaza indiferent de exceptia aparuta.");
//      }      
      finally 
      {
      System.out.println("\n\n\nASTA SE EXECUTA TOTDEAUNA: \nDar mai ales, nu uitati - Ana are mere :)!");
      
      }
   }
}