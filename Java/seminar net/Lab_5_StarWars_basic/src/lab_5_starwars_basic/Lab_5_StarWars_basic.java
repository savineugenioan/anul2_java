package lab_5_starwars_basic;

import java.util.Scanner;


class Personaj {

    //variabilele clasei, cele la care ne referim cu sau fara this. (atributele fiecarui obiect de tipul Personaj)
    private String nume;
    private int viata = 100;
    private int forta;
    //variabila statica, nu depinde de instanta
    static int nrPersonaje=0;
    
    //constructorii clasei
    
    public Personaj(String n, int d) {
        nume=n;
        forta=d;
        Personaj.nrPersonaje++;
    }
    
    public Personaj() {
            this("Vader",10);
            Personaj.nrPersonaje++;
    }
    
    public Personaj (String n) {
            nume=n;
            forta=50;
            Personaj.nrPersonaje++;
    }
    
    public void atac(Personaj p){
        if ((viata>0)&&(p.viata>0)){
            double temporar=Math.random();
            System.out.println("\nNumarul aleator generat:"+temporar);
            int lovitura=(int)((forta)*temporar);
            p.viata-=lovitura;
            System.out.println("Agresorul "+intoarceNume()+" il ataca pe "+p.intoarceNume() + " cu o forta de: "+lovitura);
            if (p.viata<=0){
                System.out.println(p.nume+" a murit!\t\tRIP :)");
                p.viata=0;
            }
        }
        else
            System.out.println(intoarceNume()+ " nu il poate ataca pe "+p.intoarceNume()+" pentru ca e deja mort!");
    }
    
    public void info(){
        System.out.println("--------------------------");
        System.out.println("Nume: "+intoarceNume());
        System.out.print("Viata: "+intoarceViata());
        if (intoarceViata()==0) System.out.print("\t...mort si ingropat :)");
        System.out.println("\nForta: "+intoarceForta());
        System.out.println("--------------------------");
    }
    
    // Modificare-2, static e pt accesarea dintr-un mediu "static", adica din metoda main
    public static int numaraPersonaje() {
//        System.out.println("~~~~~~~~~~~~~~");
//        System.out.println("In joc au existat "+nrPersonaje+" personaje");
//        System.out.println("~~~~~~~~~~~~~~");
          return nrPersonaje;
    }    
    
    public String intoarceNume() {
        return nume;
    }

    public void seteazaNume(String nume) {
        this.nume = nume;
    }

    public int intoarceViata() {
        return viata;
    }

    public int intoarceForta() {
        return forta;
    }

    public void seteazaForta(int forta) {
        this.forta = forta;
    }

}

//clasa principala, cea care contine main-ul, implements e doar pt utilizarea interfetelor
public class Lab_5_StarWars_basic  {
    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {

        //Personaj atacator = new Personaj(args[0],30);
        Personaj atacator = new Personaj("Luke",100);
        
        //Personaj atacat = new Personaj();
        //Personaj atacat = new Personaj("Gica");
        Personaj atacat = new Personaj("Vader",100);
        
        Personaj observator = new Personaj("Chewbacca",150);
        
        System.out.println("Cele doua personaje care se lupta sunt: ");
        atacat.info();
        atacator.info();
        
        System.out.println("Personajul atacat este: "+atacat.intoarceNume());
        System.out.println("Personajul agresor este: "+atacator.intoarceNume());        
        
        atacator.atac(atacat);
        
        System.out.println("\n\nStarea in care a ramas cel atacat: ");
        atacat.info();
        
        System.out.println("\n\nNumar de personaje din joc, prin variabila: "+Personaj.nrPersonaje);
        System.out.println("Numar de personaje din joc, prin metoda: "+Personaj.numaraPersonaje());
        }
    }