package lab_3_copierefisier;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream; // e o clasa ce se foloseste pe baza convertirii datele in byte si apoi scrierii lor
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 
public class Lab_3_CopiereFisier {
 
    public static void main(String[] args)
    {	
    	InputStream fluxIntrare = null;
    	OutputStream fluxIesire = null;
    	try{
 
    	    File fisier1 =new File("C:\\_Temporar\\JAVA Labs\\fisier-initial.txt");
    	    File fisier2 =new File("C:\\_Temporar\\JAVA Labs\\fisier-copie.txt");
 
    	    fluxIntrare = new FileInputStream(fisier1);
    	    fluxIesire = new FileOutputStream(fisier2); // suprascrie fisier2 totdeauna
//    	    fluxIesire = new FileOutputStream(fisier2,true); // adauga la continutul anterior din fisier2
 
    	    byte[] temporar = new byte[42]; //citim cate X bytes
            
 
    	    int lungime;
    	    while ((lungime =fluxIntrare.read(temporar)) > 0){
    	    	fluxIesire.write(temporar, 2, lungime); // scrie din tabloul de byte cate "lungime" valori, cu un offset de 2
    	    }
 
    	    if (fluxIntrare != null)fluxIntrare.close();
    	    if (fluxIesire != null)fluxIesire.close();
 
    	    System.out.println("Fisier copiat..");
    	}
        
        catch(IndexOutOfBoundsException e){
            System.out.println("Oooops... ceva nu e bine");
    	}
        
        catch(IOException e){
            System.out.println("Oooops din nou... dar vedem mai multe detalii despre eroare");
            e.printStackTrace();
    	}
    }
}