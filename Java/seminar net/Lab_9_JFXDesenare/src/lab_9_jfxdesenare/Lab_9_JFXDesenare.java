package lab_9_jfxdesenare;

import javafx.application.Application; 
import javafx.collections.ObservableList; 
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.stage.Stage; 
import javafx.scene.text.Font; 
import javafx.scene.text.Text; 
import javafx.scene.shape.Line; 
import javafx.stage.StageStyle;
         
public class Lab_9_JFXDesenare extends Application { 
   @Override 
   public void start(Stage scenaPrimara) {       
      
      //poate fi si UNDECORATED, TRANSPARENT, UTILITY, UNIFIED
      scenaPrimara.initStyle(StageStyle.DECORATED);
      
      scenaPrimara.setFullScreen(true);
      scenaPrimara.setFullScreenExitHint("Apasa ESC pentru a iesi din Full-Screen!");
      
      //crearea obiect Text 
      Text text1 = new Text(); 
      
      //crearea unei linii
      Line linie1 = new Line(); 
         
      //Setting the properties to a line 
      linie1.setStartX(50.0); 
      linie1.setStartY(170.0); 
      linie1.setEndX(350.0); 
      linie1.setEndY(170.0); 
       
      //setare Font cu marimea 45
      text1.setFont(new Font(45)); 
       
      //setarea pozitie textului 
      text1.setX(50); 
      text1.setY(150);         
	 
//setarea textului propriu-zis 
      text1.setText("Ana are mere!"); 
         
      //crearea obiectului grup 
      Group root = new Group(); 
       
      //generarea listei de noduri-copil ale radacinii 
      ObservableList lista = root.getChildren(); 
      System.out.println("Lista initiala: "+lista);
       
      //adaugarea obiectului Text ca nod-copil in obiectul Group
      lista.add(text1);       
      System.out.println("Lista la acest pas: "+lista);
      
      //adaugarea obiectului Line ca nod-copil in obiectul Group
      lista.add(linie1);      
      System.out.println("Lista: "+lista);
                     
      //crearea unui obiect Scene 
      Scene scena1 = new Scene(root, 800, 400); 
      
//setarea titlului sceneiPrimare - ferestrei 
      scenaPrimara.setTitle("Exemplu de aplicatie JavaFX"); 
         
      //adaugarea scenei la scenaPrimara 
      scenaPrimara.setScene(scena1); 
         
      //afisarea continutului sceneiPrimare 
      scenaPrimara.show(); 
   }   
   public static void main(String args[]){ 
      launch(args); 
   } 
} 