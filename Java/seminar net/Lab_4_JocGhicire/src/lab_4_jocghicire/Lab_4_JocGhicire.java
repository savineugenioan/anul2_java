package lab_4_jocghicire;
/**
 *
 * @author Alex Tabusca
 */

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Lab_4_JocGhicire {
    public static void main(String args[]){

        //facem un obiect Random pentru a folosi nextInt
        Random aleatoriu = new Random();

        // declaram intregii si obiectul pentru citit de la tastatura
        int intregAleatoriu;
        int intrareUtilizator;
        Scanner citire = new Scanner(System.in);
        
//bucla mare, pentru a continua eventual jocul si dupa ghicire
        boolean continuare=true;
        while (continuare)
        {

        // punem numarul aleatoriu ales de calculator in varianbila int
        intregAleatoriu = aleatoriu.nextInt(10)+1;
        System.out.println("Numar aleatoriu ales: "+intregAleatoriu);
        
        while(true){
            System.out.print("Alege un numar: ");
            // preluam ce zice utilizatorul direct intr-o variabil intreaga
            
            try {
            intrareUtilizator = citire.nextInt();
            }
            catch(InputMismatchException eroare) {
            System.out.println("Am zis NUMAR!... revino cand sti ce e un numar :).\nPa. ");
            break;
            }
                        
            //verificam numerele si dam mesaje
            if(intrareUtilizator==intregAleatoriu){
                System.out.println("\n!!! Nostradamus de Dambovita!");
                break;
            }else if(intrareUtilizator>intregAleatoriu){
                System.out.println("Cauta un numar mai mic");
            }else{
                System.out.println("Cauta un numar mai mare");
            }
        }
        
        String optiuneContinuare;
        System.out.println("\nSe mai bat in continuare? (da/nu) ");
        Scanner citireOptiune = new Scanner(System.in);
        optiuneContinuare = citireOptiune.next();
        switch (optiuneContinuare)
        {
            case "da": System.out.println("Ok - la lupta baieti...");break;
            case "nu": System.out.println("Ok - destul. Forta fie cu voi! Pa."); continuare=false; break;
            default: System.out.println("Nu esti prea hotarat... ii lasam sa se mai lupte putin... :)");
        }
        }
    }
}