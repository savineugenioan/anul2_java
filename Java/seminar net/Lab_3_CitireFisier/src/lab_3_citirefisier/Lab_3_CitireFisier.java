package lab_3_citirefisier;
/**
 *
 * @author Alex Tabusca
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Lab_3_CitireFisier {

 public static void main(String[] args) {
  BufferedReader obiectCititTemporar = null;
  
  try {
   String sirLiniaCurenta;
   FileReader numeFisier = new FileReader("C:\\_Temporar\\JAVA Labs\\fisier.txt");
   obiectCititTemporar = new BufferedReader(numeFisier);
//   putem inlocui liniile de mai sus cu:
//   obiectCititTemporar = new BufferedReader(new FileReader("C:\\_Temporar\\JAVA Labs\\fisier.txt"));
   
   System.out.println("Aici incepe fisierul citit:\n______");
   while ((sirLiniaCurenta = obiectCititTemporar.readLine()) != null) {
    System.out.println(sirLiniaCurenta);
   }
   System.out.println("______\nAici s-a terminat fisierul citit");
  } 
  catch (IOException e) {
      System.out.println("poc! - nu e fisierul acolo");
  } 
  finally {
   System.out.println("\n\nVa dorim oricum o zi buna!\u2764 \u2665 \u2765");
  }
 }
}