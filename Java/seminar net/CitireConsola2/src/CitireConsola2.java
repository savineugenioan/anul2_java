import java.io.*;

public class CitireConsola2 
{
    public static void main(String[] args) 
    {
      System.out.println("Exemplu citire text din consola");
    
      //folosim System.in pt a introduce siruri de la consola
      try
      {
      
      //Trebuie sa facem un obiect BufferedReader, pentru care vom apela System.in ca sa preluam input de la utilizator
      
      
      InputStreamReader obiectIntrodus = new InputStreamReader(System.in);
      BufferedReader temporar = new BufferedReader(obiectIntrodus);
      
      // sau putem scurta codul inlocuind liniile 16 si 17 cu 20, comentata mai jos
      //BufferedReader temporar = new BufferedReader(new InputStreamReader(System.in));
      
      
      String textUtilizator;
      System.out.println("Tastati 'exit' pentru a termina introducere.");
      System.out.println("* * * *");
      System.out.println("Introdu text: ");
      
      
        do {
          //cititorul .readLine() asteapta ca utilizatorul sa introduca siruri de caractere
          //cand apasati pe enter se va si afisa ce ati introdus dar va continua si introducerea
          //input-ul este pastrat in buffer pana cand se indeplineste conditia de iesire
          textUtilizator = (String) temporar.readLine();
          System.out.println("Ati introdus textul:");
          System.out.println(textUtilizator);
          System.out.println("---------------------------");
        } while(!textUtilizator.equals("exit"));
      }
      catch(Exception e)
      {
          System.out.println("...ooops, am dat de o exceptie!");
      }
    }
}