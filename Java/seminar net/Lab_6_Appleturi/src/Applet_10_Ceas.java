import java.awt.*;
import java.applet.*;
import java.util.*;

public class Applet_10_Ceas extends Applet implements Runnable { 
   Thread t,t1;
   
//   public void init() {  
//      setBackground( Color.green);  
//   }    
   
   public void start() {
      t = new Thread(this);
      t.start();
   }
   public void run() { 
      t1 = Thread.currentThread();
      while(t1 == t) {
         repaint();
         try {
            t1.sleep(1000);    
         }
         catch(InterruptedException e){}
      }
   }
   public void paint(Graphics g) {
      Calendar cal = new GregorianCalendar();
      String ore = String.valueOf(cal.get(Calendar.HOUR));
      String minute = String.valueOf(cal.get(Calendar.MINUTE));
      String secunde = String.valueOf(cal.get(Calendar.SECOND));
      g.drawString(ore + ":" + minute + ":" + secunde, 20, 30);
   }
}