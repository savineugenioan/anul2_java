import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Color;
  
public class Applet_6_Linii extends Applet{
 
        public void paint(Graphics g){
                g.setColor(Color.red);
                g.drawLine(10,10,50,50);
               
                //linie verticala
                g.setColor(Color.yellow);
                g.drawLine(10,50,10,100);
               
                //linie orizontala
                g.setColor(Color.blue);
                g.drawLine(10,10,50,10);
        }
}