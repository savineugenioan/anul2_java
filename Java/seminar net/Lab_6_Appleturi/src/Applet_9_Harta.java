import java.awt.*;
import java.awt.geom.*;

public class Applet_9_Harta extends java.applet.Applet {
 @Override
 public void paint(Graphics ecran) {
  Graphics2D ecran2D=(Graphics2D)ecran;
  setBackground(Color.blue);
//Desenare valuri
ecran2D.setColor(Color.white);
BasicStroke creion=new BasicStroke(2F,BasicStroke.CAP_BUTT,BasicStroke.JOIN_ROUND);
ecran2D.setStroke(creion);
for (int ax=10;ax<340;ax+=10) 
 for (int ay=30;ay<340;ay+=10){
  Arc2D.Float val=new Arc2D.Float(ax,ay,10,10,0,-180,Arc2D.OPEN);
 ecran2D.draw(val);
}
//Desenare harta
GradientPaint degrade=new GradientPaint(0F,0F,Color.green,50F,50F,Color.orange,true);
ecran2D.setPaint(degrade);
GeneralPath f1=new GeneralPath();
f1.moveTo(10F,12F);
f1.lineTo(234F,15F);
f1.lineTo(253F,25F);
f1.lineTo(261F,71F);
f1.lineTo(344F,209F);
f1.lineTo(336F,278F);
f1.lineTo(295F,310F);
f1.lineTo(259F,274F);
f1.lineTo(205F,188F);
f1.lineTo(211F,171F);
f1.lineTo(195F,174F);
f1.lineTo(191F,118F);
f1.lineTo(120F,56F);
f1.lineTo(94F,68F);
f1.lineTo(81F,49F);
f1.lineTo(12F,37F);
f1.closePath();
ecran2D.fill(f1);
//Desenare elipse
ecran2D.setColor(Color.black);
BasicStroke creion2=new BasicStroke();
ecran2D.setStroke(creion2);
Ellipse2D.Float e1=new Ellipse2D.Float(235,140,15,15);
Ellipse2D.Float e2=new Ellipse2D.Float(225,130,15,15);
Ellipse2D.Float e3=new Ellipse2D.Float(245,130,15,15);
ecran2D.fill(e1);
ecran2D.fill(e2);
ecran2D.fill(e3);
}
}