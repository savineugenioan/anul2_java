import java.awt.*;
import java.applet.*;

public class Applet_12_ReclamaColor extends Applet implements Runnable {
   String str = "Acesta este un inceput banal de banner :)... ";
   Thread t ;
   boolean b;
   
   public void init() {
      setBackground(Color.red);
      setForeground(Color.yellow);
   }
   public void start() {
      t = new Thread(this);
      b = false;
      t.start();
   }
   public void run () {
      char ch;
      for( ; ; ) {
      try {
         repaint();
         Thread.sleep(250);
         ch = str.charAt(0);
         str = str.substring(1, str.length());
         str = str + ch;
      }
      catch(InterruptedException e) {}
      }
   }
   public void paint(Graphics g) {
      //g.drawRect(1,1,30,50);
      g.setColor(Color.yellow);
      g.fillRect(0,75,300,50);
      g.setColor(Color.blue);
      g.drawString(str, 1, 105);
   }
}