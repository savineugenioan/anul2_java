/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alex Tabusca
 */
import java.awt.Font; 
import java.awt.Graphics;
import java.awt.FontMetrics;

public class Applet_5_TextCentrat extends java.applet.Applet {

public void paint(Graphics ecran) {
Font f=new Font("Calibri",Font.BOLD,18);
FontMetrics fm=getFontMetrics(f);
ecran.setFont(f);
String s="La revedere.";
int x=(getSize().width-fm.stringWidth(s))/2;
int y=getSize().height/2;
ecran.drawString(s,x,y);
}
}