import java.applet.*; 
import java.awt.*;

public class Applet_8_Imagine extends Applet { 
   Image img;
   public void paint(Graphics g) {
      img = getImage(getCodeBase(), "IoT_image.jpg");
      g.drawImage(img, 0, 0, this);
   } 
}