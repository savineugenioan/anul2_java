
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author STT2
 */
public class SnakeRawVersion extends JFrame implements Runnable{
    int snakeX=-1,snakeY=-1,snakeD=10,appleX,appleY,appleD=6;
    int[] snake_x_=new int[900],snake_y_=new int[900];
    int xDir,yDir;
    int score=0;
    int dots=1;
    boolean x=true;
    JPanel drawArea = new JPanel(){
        public void paintComponent(Graphics g){
            super.paintComponent(g);
            for(int z=0;z<dots;z++)
                g.fillOval(snake_x_[z], snake_y_[z], snakeD, snakeD);
            //g.fillOval(snakeX, snakeY, snakeD, snakeD);
            g.fillOval(appleX, appleY, appleD, appleD);
            g.drawString("scor "+score, getWidth()-50, getHeight()-10);
            if(x==false)
                g.drawString("Ati Pierdut !", getWidth()/2, getHeight()/2);
        }  
    };
   public void init()
   {
        snake_x_[0]=1;
        snake_y_[0]=1;
   }
    public SnakeRawVersion()
    {
        super("snake");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,350);
        drawArea.setSize(300, 300);
        //snake_x_[0]=snakeX;
        //snake_y_[0]=snakeY;
        setLayout(new GridLayout(1,1));
        add(drawArea);
        
        addKeyListener(new KeyAdapter(){
        public void keyPressed(KeyEvent e){
            int code=e.getKeyCode();
            switch(code){
                case KeyEvent.VK_LEFT: if(xDir!=1 && (snake_x_[0]!=snakeX || snake_y_[0]!=snakeY)){xDir=-1;yDir=0;snakeX=snake_x_[0];
            snakeY = snake_y_[0];}break;
                case KeyEvent.VK_RIGHT: if(xDir!=-1 && (snake_x_[0]!=snakeX || snake_y_[0]!=snakeY)){xDir=1;yDir=0;snakeX=snake_x_[0];
            snakeY = snake_y_[0];}break;
                case KeyEvent.VK_UP: if(yDir!=1 && (snake_x_[0]!=snakeX || snake_y_[0]!=snakeY)){xDir=0;yDir=-1;snakeX=snake_x_[0];
            snakeY = snake_y_[0];}break;
                case KeyEvent.VK_DOWN: if(yDir!=-1 && (snake_x_[0]!=snakeX || snake_y_[0]!=snakeY)){xDir=0;yDir=1;snakeX=snake_x_[0];
            snakeY = snake_y_[0];}break;
            }
        }
    });
        
        setVisible(true);
            appleX=(int)(Math.random()*(drawArea.getWidth()-2*appleX));
            appleY=(int)(Math.random()*(drawArea.getWidth()-2*appleY));
        drawArea.repaint();
    }
    public void run(){
        while(x){
            //System.out.println(appleX + " "+ appleY);
            for(int z=1;z<dots;z++)
                 if((snake_x_[0]==snake_x_[z]) && (snake_y_[0]==snake_y_[z]) && dots>4)
                 {
                     x=false;
                     System.out.print("*");
                     drawArea.repaint();
                     break;
                 }
            for(int z=dots-1;z>=1;z--){
                snake_x_[z]=snake_x_[z-1];
                snake_y_[z]=snake_y_[z-1];
            }
            snake_x_[0]+=xDir*snakeD+xDir;
            snake_y_[0]+=yDir*snakeD+yDir;
            //if(Math.sqrt(Math.pow(snakeX+snakeD/2-appleX-appleD/2,2)+Math.pow(snakeY+snakeD/2-appleY-appleD/2,2))<(snakeD+appleD)/2)
            if(Math.sqrt(Math.pow(snake_x_[0]+snakeD/2-appleX-appleD/2,2)+Math.pow(snake_y_[0]+snakeD/2-appleY-appleD/2,2))<(snakeD+appleD)/2)
            { // daca sarpele cu marul se intersecteaza
                appleX=(int)(Math.random()*(drawArea.getWidth()-2*appleD));
                appleY=(int)(Math.random()*(drawArea.getHeight()-2*appleD));
                score++;
                snake_x_[dots]=snake_x_[dots-1]-xDir*snakeD-xDir;///
                snake_y_[dots]=snake_y_[dots-1]+yDir*snakeD+yDir;///
                dots++;
            }
            for(int bn=0;bn<dots;bn++)
               System.out.print("["+snake_x_[bn] + " " +snake_y_[bn]+"]"+"  "+"   "+snakeX+" "+snakeY);
            System.out.println("");
            
            try{
                Thread.sleep(100);
            }
            catch(InterruptedException E){}
            if(snake_x_[0]<-snakeD/2 || snake_x_[0]>drawArea.getWidth()-snakeD/2 || snake_y_[0]<-snakeD/2  || snake_y_[0]>drawArea.getHeight()-snakeD/2)
            {
                x=false;
                drawArea.repaint();
                break;
            }
            //snakeX=snake_x_[0];
            //snakeY = snake_y_[0];
            drawArea.repaint();
        }
    }
    public static void main(String[] args){
        new Thread(new SnakeRawVersion()).start();
}
}
